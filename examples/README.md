

# Smallest file

```sh
cargo run --release -- -f examples/*.sh
```

# With logging

```sh
cargo run --release --features logging -- -f examples/*.sh
```