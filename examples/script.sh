#!/bin/sh

echo "Hi, it's $0 $*." >&2
echo "Type something:" >&2
read -r input
echo "I thought you'd say something like that. '$input' ... what else could you come up with?"
