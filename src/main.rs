use anyhow::Context;
use std::env;
use std::fmt::Display;
use std::fs;
use std::io::{self, Read};
use std::path::PathBuf;
use std::process;

#[cfg(feature = "logging")]
use log::*;
#[cfg(not(feature = "logging"))]
use self::silence as trace;
#[cfg(not(feature = "logging"))]
use std::{eprintln as info, eprintln as error};

#[allow(unused_macros)]
#[macro_export]
macro_rules! silence {
    ($ignore:tt) => {};
}

/// shelfie turns shell scripts into ELFs so you can set capabilities, setuid and setgid.
///
/// Compile either traditionally with `cargo build --release`,
/// or in the field with `rustc -O -C "strip=symbols" src/main.rs -o shelfie`.
/// though `rustc src/main.rs` would work just fine except for the file size.
///
/// Vanila shelfie produces .shelf files
///  composed of itself, a marker and the script concatenated.
/// The .shelf executable then finds the embedded script 
///  and passes it to `/bin/sh -c -p` as an argument on execution.
/// No attempts are made to obfuscate the script in the binary.
fn main() {
    #[cfg(feature = "logging")]
    env_logger::builder()
        .filter_level(LevelFilter::Info)
        .parse_default_env()
        .init();

    let result = run();

    if let Err(e) = result {
        error!("{:?}", e);
        process::exit(1);
    }
}
fn run() -> anyhow::Result<()> {
    let path_in = env::current_exe().context("getting exe path")?;
    let path_out = path_in.with_extension("shelf");
    let file = fs::File::open(&path_in).context(format!("opening {path_in:?}"))?;
    let map = unsafe {
        // safeish somewhat... assuming it's read-only access here
        // we search for a marker, then the position must match
        // and then we use if for bufferless file copy
        // or we read the script string from it (utf-8)
        memmap2::Mmap::map(&file)
    }
    .context(format!("mmapping {path_in:?}"))?;
    if map.len() >= u32::MAX as usize {
        anyhow::bail!("ELF size {} over board!", map.len());
    }

    if let Some(script) = get_script(&map)? {
        let status = process::Command::new("/bin/sh")
            .arg("-c")
            .arg("-p")
            .arg(script)
            .args(env::args())
            .status()?;

        process::exit(
            status
                .code()
                .unwrap_or(if status.success() { 0 } else { 1 }),
        )
    } else {
        let opt = Opt::from_args();
        for script in &opt.scripts {
            if let Some("-") = script.to_str() {
                produce(&*map, &mut io::stdin(), &path_out, &opt)?;
            } else {
                let script_file = fs::File::open(&script).context(format!("reading {script:?}"))?;
                let script_map = unsafe {
                    // safe - it's only used for bufferless file copy
                    memmap2::Mmap::map(&script_file)
                }
                .context(format!("mmapping {script:?}"))?;
                produce(
                    &*map,
                    &*script_map,
                    PathBuf::from(script).with_extension("shelf"),
                    &opt,
                )?;
            }
        }
        Ok(())
    }
}

const MARKER: &str = "###shelfmarker=";

fn produce(
    mut elfile: impl Read,
    mut script: impl Read,
    dst: impl AsRef<std::path::Path>,
    opt: &Opt,
) -> anyhow::Result<()> {
    let dst = dst.as_ref();
    let ctx = |msg| format!("{msg} to {dst:?}");
    let mut fsopts = fs::OpenOptions::new();
    if opt.force {
        fsopts.create(true).truncate(true);
    } else {
        fsopts.create_new(true);
    }
    let mut shelf = fsopts.write(true).open(dst).context(ctx("writing shelf"))?;
    let pos = io::copy(&mut elfile, &mut shelf).context(ctx("writing elf"))?;
    io::copy(&mut MARKER.as_bytes(), &mut shelf).context(ctx("writing marker"))?;
    io::copy(&mut format!("{:08X}\n", pos).as_bytes(), &mut shelf).context(ctx("writing pos"))?;
    io::copy(&mut script, &mut shelf).context(ctx("writing script"))?;
    #[cfg(unix)]
    {
        use std::os::unix::prelude::PermissionsExt;
        let mut p = shelf.metadata()?.permissions();
        p.set_mode(p.mode() | 0o111);
        fs::set_permissions(dst, p)?;
    }
    info!("Produced {dst:?}");
    Ok(())
}

fn get_script(elfile: &[u8]) -> anyhow::Result<Option<&str>> {
    let split = match get_split(elfile) {
        Some(script) => script,
        None => return Ok(None),
    };
    let script = std::str::from_utf8(&elfile[split..]).context("script must be utf-8")?;
    Ok(Some(script))
}
fn get_split(elfile: &[u8]) -> Option<usize> {
    let mut split = None;
    for start in 0..elfile.len() {
        split = get_split_here(elfile, start);
        if split.is_some() {
            break;
        }
    }
    split
}
fn get_split_here(elfile: &[u8], start: usize) -> Option<usize> {
    let here = &elfile[start..];
    if !here.starts_with(MARKER.as_bytes()) {
        return None;
    }
    trace!("marker @{start}");
    let pos = here[MARKER.len()..].get(0..8)?;
    trace!("pos {pos:?}");
    let pos = std::str::from_utf8(pos).ok()?;
    trace!("pos {pos:?}");
    let pos = usize::from_str_radix(pos, 16).ok()?;
    trace!("pos {pos}");
    if start != pos {
        return None;
    }
    Some(pos + MARKER.len() + 8 + 1)
}

/// - turns shell scripts into ELFs. Gimme some ...
///
/// It will write ["<script>.shelf"]...
#[derive(Default)]
struct Opt {
    pub name: String,
    pub version: &'static str,
    pub scripts: Vec<PathBuf>,
    pub force: bool,
    pub show_help: bool,
    pub show_version: bool,
    pub error: String,
}
impl Opt {
    pub fn from_args() -> Self {
        let mut options_done = false;
        let mut args = env::args();
        let mut me = Self::default();
        me.version = option_env!("CARGO_PKG_VERSION").unwrap_or("0.0.0");
        me.name = args.next().unwrap_or("shelfie".to_owned());
        while let Some(arg) = args.next() {
            if options_done {
                me.scripts.push(PathBuf::from(arg))
            } else {
                match arg.as_bytes() {
                    b"-h" | b"--help" => me.show_help = true,
                    b"-V" | b"--version" => me.show_version = true,
                    b"-f" | b"--force" => me.force = true,
                    b"--" => options_done = true,
                    [b'-', ..] => {
                        me.error = format!("Invalid option {arg:?}.");
                        break;
                    }
                    _ => me.scripts.push(PathBuf::from(arg)),
                }
            }
        }

        if me.show_version {
            println!("{}", me.version);
            process::exit(0);
        }
        if me.show_help {
            println!("{me}");
            process::exit(0);
        }
        if me.scripts.is_empty() || !me.error.is_empty() {
            eprintln!("{me}");
            process::exit(1);
        }
        me
    }
}

impl Display for Opt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.show_help {
            // ignore error and no scripts
        } else if !self.error.is_empty() {
            writeln!(f, "{}\n", self.error)?;
        } else if self.scripts.is_empty() {
            writeln!(f, "No scripts given to convert.\n")?;
        }

        write!(f,"shelfie {v} turns shell scripts into ELFs. Gimme some ...

It will write ['<script>.shelf']...

USAGE:
    {me} [FLAGS] <script>...

FLAGS:
    -f, --force      
            Overwrite existing shelf files

    -h, --help       
            Prints help information

    -V, --version    
            Prints version information

ARGS:
    <script>...    
            Shell script to turn into an ELF. Use '-' to read script from stdin and store it in '<self>.shelf'
", v=self.version, me=self.name)
    }
}
