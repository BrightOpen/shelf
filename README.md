# shelfie turns shell scripts into ELFs

Like [`shc`](https://en.wikipedia.org/wiki/Shc_(shell_script_compiler)) would.
No attempts are made to obfuscate the script, quite the contrary.
The result is not as a minimal a file and it's only been tested with my version of dash. 
On the plus side, it's Rusty and easy to inspect, validate, etc...
See the [examples](examples/) for more insights.


## Usage

Install the tool locally with

```
cargo install shelfie
```

Then you can turn sh into ELF

```
shelfie your-fav.sh
```

Help is available

```
shelfie --help
```

## How

I contemplated about mastering the ELF-fu and gave up. Instead, we simply append the script to the ELF with a marker. At run time, we scan the executable for signs of the marker, extract the script and pass it to `/bin/sh`.

## Why

Having an ELF is useful for setuid, setguid, set capabilities and such rooty magyc as you can't do those things with interpretted scripts. And having shell script is good for quick hacking, right? You could do it with shc or similar. No offence if you pick another tool. This is a Rust exercise with memory mapped files.

